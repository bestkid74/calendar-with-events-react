import React, { Component } from 'react';

class Weekdays extends Component {

    render() {
        const days = this.props.dayNames;
        const daysItems = days.map((day, index) => <div className="r-cell" key={index}>{day}</div>);

        return (
            <div className="r-row r-weekdays">
                {daysItems}
            </div>
        )
    }
}

export default Weekdays;