import React, { Component } from 'react';
import { Grid, Row, Modal, Button, Form, FormGroup, FormControl } from 'react-bootstrap';

import Header from './Header';
import Weekdays from './Weekdays';
import Monthdates from './Monthdates';

class Calendar extends Component {
    constructor(props) {
        super(props)

        const date = new Date();

        this.state = {
            year: date.getFullYear(),
            month: date.getMonth(),
            startDay: 1,
            dayNames: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
            monthNamesFull: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            firstOfMonth: new Date(date.getFullYear(), date.getMonth(), 1),
            daysInMonth: new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate(),
            isModalOpen: false,
            dayEvent: '',
            currentDay: '',
            events: {}
        };

        this.getPrev = this.getPrev.bind(this);
        this.getNext = this.getNext.bind(this);
        this.selectDate = this.selectDate.bind(this);
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.onSaveEvent = this.onSaveEvent.bind(this);
        this.onChangeEvent = this.onChangeEvent.bind(this);
        this.onDeleteEvent = this.onDeleteEvent.bind(this);
    }

    componentWillMount() {
        this.setState({
            events: localStorage.getItem("events") ? JSON.parse(localStorage.getItem("events")) : {}
        });
    }

    getPrev() {
        if (this.state.month > 0) {
            this.setState({
                firstOfMonth: new Date(this.state.year, this.state.month - 1, 1),
                daysInMonth: new Date(this.state.year, this.state.month, 0).getDate(),
                month: this.state.month - 1,
            });
        } else {
            this.setState({
                firstOfMonth: new Date(this.state.year, 11, 1),
                daysInMonth: new Date(this.state.year, 12, 0).getDate(),
                month: 11,
                year: this.state.year - 1
            });
        }
    }

    getNext() {
        if (this.state.month < 11) {
            this.setState({
                firstOfMonth: new Date(this.state.year, this.state.month + 1, 1),
                daysInMonth: new Date(this.state.year, this.state.month + 2, 0).getDate(),
                month: this.state.month + 1
            });
        } else {
            this.setState({
                firstOfMonth: new Date(this.state.year, 0, 1),
                daysInMonth: new Date(this.state.year, 1, 0).getDate(),
                month: 0,
                year: this.state.year + 1
            });
        }
    }

    selectDate(e) {
        const currentDay = e.currentTarget.getAttribute('data-value');
        const dayEvent = this.state.events[`${this.state.year}-${this.state.month}-${currentDay}`]
            ? this.state.events[`${this.state.year}-${this.state.month}-${currentDay}`] : '';
        this.setState({
            dayEvent: dayEvent,
            currentDay: currentDay
        });

        this.showModal();
    }

    showModal() {
        this.setState({
            isModalOpen: true
        })
    }

    hideModal() {
        this.setState({
            isModalOpen: false
        })
    }

    onSaveEvent(e) {
        e.preventDefault();
        let events = this.state.events;

        if (this.state.dayEvent.trim() !== '') {
            events[`${this.state.year}-${this.state.month}-${this.state.currentDay}`] = this.state.dayEvent;
            localStorage.setItem('events', JSON.stringify(this.state.events));
            this.setState({
                events: events
            });
        }

        this.hideModal();
    }

    onChangeEvent(event) {
        const target = event.target;
        const value = target.value;

        this.setState({
            dayEvent: value
        })
    }

    onDeleteEvent() {
        let events = this.state.events;

        delete events[`${this.state.year}-${this.state.month}-${this.state.currentDay}`];
        localStorage.setItem('events', JSON.stringify(this.state.events));
        this.setState({
            events: events
        });

        this.hideModal();
    }

    render() {
        return (
            <Grid>
                <h1 className='text-center'>Календарь</h1>
                <div className = 'custom' >
                    <Row className="show-grid">
                        <div className="r-calendar">
                            <div className="r-inner">

                                <Header monthNames={this.state.monthNamesFull}
                                        month={this.state.month}
                                        year={this.state.year}
                                        onPrev={this.getPrev}
                                        onNext={this.getNext} />

                                <Weekdays dayNames={this.state.dayNames} />

                                <Monthdates month={this.state.month}
                                            year={this.state.year}
                                            daysInMonth={this.state.daysInMonth}
                                            firstOfMonth={this.state.firstOfMonth}
                                            startDay={this.state.startDay}
                                            onSelect={this.selectDate}
                                            dayHasEvents={this.state.events}/>

                                <Modal show={this.state.isModalOpen} onHide={this.hideModal}>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Событиe:</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <Form horizontal onSubmit={this.onSaveEvent}>
                                            <FormGroup controlId="formHorizontalEvent">
                                                <FormControl type="text"
                                                             name="event"
                                                             value={this.state.dayEvent}
                                                             onChange={this.onChangeEvent}/>
                                            </FormGroup>
                                        </Form>
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button bsStyle='warning' onClick={this.onSaveEvent}>
                                            Сохранить
                                        </Button>
                                        <Button bsStyle='danger' onClick={this.onDeleteEvent}>
                                            Удалить
                                        </Button>
                                        <Button onClick={this.hideModal}>Отмена</Button>
                                    </Modal.Footer>
                                </Modal>
                            </div>
                        </div>
                    </Row>
                </div>
            </Grid>
        )
    }
}

export default Calendar;