import React, { Component } from 'react';

class Monthdates extends Component {

    render() {
        const today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        const weekStack = Array.apply(null, {length: 7}).map(Number.call, Number);
        const startDay = this.props.firstOfMonth.getUTCDay();
        const first = this.props.firstOfMonth.getDay();

        let rows = 5;
        if ((startDay === 5 && this.props.daysInMonth === 31) || (startDay === 6 && this.props.daysInMonth > 29)) {
            rows = 6;
        }

        let className = rows === 6 ? 'r-dates' : 'r-dates r-fix';
        const haystack = Array.apply(null, {length: rows}).map(Number.call, Number);
        let day = this.props.startDay + 1 - first;
        while (day > 1) {
            day -= 7;
        }
        day -= 1;

        return (
            <div className={className}>
                { haystack.map( (item, i) => {
                    let d = day + i * 7;
                    return (
                        <div className="r-row" key={i}>
                            <div className='r-cell r-weeknum'>
                                {weekStack.map((item, i) => {
                                    d += 1;
                                    let isDate = d > 0 && d <= this.props.daysInMonth;
                                    if (isDate) {
                                        let current = new Date(this.props.year, this.props.month, d);
                                        className = current !== today ? 'r-cell r-date' : 'r-cell r-date r-today';
                                        for (let eventDate in this.props.dayHasEvents) {
                                            if (eventDate === `${this.props.year}-${this.props.month}-${d}`) {
                                                className += ' event';
                                            }
                                        }
                                        return (
                                            <div className={className}
                                                 key={i}
                                                 role='button'
                                                 tabIndex='0'
                                                 data-value={d}
                                                 onClick={this.props.onSelect}>
                                                {d}
                                            </div>
                                        );
                                    }
                                    return <div className='r-cell' key={i}/>;
                                })}
                            </div>
                        </div>
                    );
                })}
            </div>
        );
    }
}

export default Monthdates;