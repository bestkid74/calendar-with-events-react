import React, { Component } from "react";

class Header extends Component {
    render() {
        return (
            <div className="r-row r-head">
                <div className="r-cell r-prev"
                     onClick={this.props.onPrev}
                     role="button"
                     tabIndex="0" />
                <div className="r-cell r-title">
                    {`${this.props.monthNames[this.props.month]} ${this.props.year}`}
                </div>
                <div className="r-cell r-next"
                     onClick={this.props.onNext}
                     role="button"
                     tabIndex="0" />
            </div>
        );
    }
}

export default Header;