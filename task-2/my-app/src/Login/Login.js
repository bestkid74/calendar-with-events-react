import React, { Component } from 'react';
import { Row, Form, FormGroup, Col, FormControl, ControlLabel, Button, Alert } from 'react-bootstrap';
import Calendar from '../Calendar/Calendar';


class Login extends Component {

    constructor(props){
        super(props);

        this.state = {
            logged: false,
            email: '',
            password: '',
            user: {},
            message: '',
            showAlert: false,
            validationEmail: null,
            validationPassword: null,
            emailErrorMsg: '',
            passwordErrorMsg: '',
            validEmail: false,
            validPassword: false
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleDismiss = this.handleDismiss.bind(this);
    }

    componentDidMount() {
        const registeredUser = localStorage.getItem('registeredUser');

        if (registeredUser) {
            this.setState({
                user: JSON.parse(registeredUser)
            });
        }
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        if (name === 'password') {
            if (value.length < 3) {
                this.setState({
                    validationPassword: 'error',
                    passwordErrorMsg: 'Пароль должен быть не менее 3 символов!',
                    validPassword: false
                })
            } else {
                this.setState({
                    validationPassword: null,
                    passwordErrorMsg: '',
                    validPassword: true
                })
            }
        }

        if (name === 'email') {
            let matchEmails = value.match(/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/g);
            if (!matchEmails) {
                this.setState({
                    validationEmail: 'error',
                    emailErrorMsg: 'Введите корректный емейл!',
                    validEmail: false
                })
            } else {
                this.setState({
                    validationEmail: null,
                    emailErrorMsg: '',
                    validEmail: true
                })
            }
        }

        this.setState({
           [name]: value
        });
    }

    handleSubmit(event){
        event.preventDefault();
        const user = this.state.user;

        if (user.email && user.password) {
            if (user.email === this.state.email && user.password === this.state.password) {
                this.setState({
                    logged: true
                });
            } else {
                this.setState({
                    showAlert: true
                });
            }
        } else {
            localStorage.setItem('registeredUser', JSON.stringify({
                'email':this.state.email,
                'password':this.state.password
            }));

            this.setState({
                logged: true
            });
        }
    }

    handleDismiss() {
        this.setState({ showAlert: false });
    }

    render() {
        return (
            this.state.logged ? <Calendar/> : (
                <div>
                    <h1 className='text-center'>Форма логина</h1>
                    <div className = 'custom' >
                        <Row className="show-grid">
                            { this.state.showAlert &&
                                <Alert bsStyle="danger" onDismiss={this.handleDismiss}>
                                    <h4 className='text-center'>Доступ запрещен!</h4>
                                </Alert>
                            }
                            <Form horizontal onSubmit={this.handleSubmit}>
                                <FormGroup controlId="formHorizontalEmail"
                                           validationState={this.state.validationEmail}>
                                    <Col componentClass={ControlLabel} sm={2}>
                                        Емейл
                                    </Col>
                                    <Col sm={10}>
                                        <FormControl type="email"
                                                     name="email"
                                                     value={this.state.email}
                                                     onChange={this.handleChange} />
                                        <ControlLabel>{this.state.emailErrorMsg}</ControlLabel>
                                    </Col>
                                </FormGroup>

                                <FormGroup controlId="formHorizontalPassword"
                                           validationState={this.state.validationPassword}>
                                    <Col componentClass={ControlLabel} sm={2}>
                                        Пароль
                                    </Col>
                                    <Col sm={10}>
                                        <FormControl type="password"
                                                     name="password"
                                                     value={this.state.password}
                                                     onChange={this.handleChange} />
                                        <ControlLabel>{this.state.passwordErrorMsg}</ControlLabel>
                                    </Col>
                                </FormGroup>

                                <FormGroup>
                                    <Col smOffset={2} sm={10}>
                                        <Button type='submit'
                                                bsStyle='primary'
                                                disabled={!this.state.validEmail || !this.state.validPassword}>
                                            Войти
                                        </Button>
                                    </Col>
                                </FormGroup>
                            </Form>
                        </Row>
                    </div>
                </div>
            )
        )
    }
}

export default Login;
